define(function() {
  'use strict';

  // Use polyfill for setImmediate for performance gains
  var asap = window.setImmediate || function(fn) { setTimeout(fn, 1); };

  // Polyfill for Function.prototype.bind
  function bind(fn, thisArg) {
    return function() {
      fn.apply(thisArg, arguments);
    };
  }

  var isArray = Array.isArray || function(value) { return Object.prototype.toString.call(value) === '[object Array]'; };

  function handle(self, deferred) {

    if (self._state === null) {
      self._deferreds.push(deferred);
      return;
    }

    asap(function() {
      var callback, value;

      callback = self._state ? deferred.onFulfilled : deferred.onRejected;

      if (callback === null) {
        (self._state ? deferred.resolve : deferred.reject)(self._value);
        return;
      }

      try {
        value = callback(self._value);
      }
      catch (ex) {
        deferred.reject(ex);
        return;
      }

      deferred.resolve(value);

    });
  }

  function PolyfillPromise(fn) {

    var self = this;

    if (typeof this !== 'object') {
      throw new TypeError('Promises must be constructed via new');
    }

    if (typeof fn !== 'function') {
      throw new TypeError('not a function');
    }

    this._state = null;
    this._value = null;
    this._deferreds = [];

    function resolve(newValue) {
      //Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
      try {

        if (newValue === self) {
          throw new TypeError('A promise cannot be resolved with itself.');
        }

        if (newValue && (typeof newValue === 'object' || typeof newValue === 'function')) {
          var then = newValue.then;
          if (typeof then === 'function') {
            doResolve(bind(then, newValue), bind(resolve, self), bind(reject, self));
            return;
          }
        }

        self._state = true;
        self._value = newValue;
        finale();

      } catch (ex) {
        reject(ex);
      }
    }

    function reject(newValue) {
      self._state = false;
      self._value = newValue;
      finale();
    }

    function finale() {
      for (var i = 0, len = self._deferreds.length; i < len; i++) {
        handle(self, self._deferreds[i]);
      }
      self._deferreds = null;
    }

    /**
     * Take a potentially misbehaving resolver function and make sure
     * onFulfilled and onRejected are only called once.
     *
     * Makes no guarantees about asynchrony.
     */
    function doResolve(fn, onFulfilled, onRejected) {
      var done = false;
      try {
        fn(function(value) {
          if (done) {
            return;
          }
          done = true;
          onFulfilled(value);
        }, function(reason) {
          if (done) {
            return;
          }
          done = true;
          onRejected(reason);
        });
      } catch (ex) {
        if (done) {
          return;
        }
        done = true;
        onRejected(ex);
      }
    }

    doResolve(fn, bind(resolve, this), bind(reject, this));
  }

  function Handler(onFulfilled, onRejected, resolve, reject){
    this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
    this.onRejected = typeof onRejected === 'function' ? onRejected : null;
    this.resolve = resolve;
    this.reject = reject;
  }

  PolyfillPromise.prototype['catch'] = function(onRejected) {
    return this.then(null, onRejected);
  };

  PolyfillPromise.prototype.then = function(onFulfilled, onRejected) {
    var self = this;
    return new PolyfillPromise(function(resolve, reject) {
      handle(self, new Handler(onFulfilled, onRejected, resolve, reject));
    });
  };

  PolyfillPromise.all = function() {
    var args = Array.prototype.slice.call(arguments.length === 1 && isArray(arguments[0]) ? arguments[0] : arguments);

    return new PolyfillPromise(function(resolve, reject) {
      if (args.length === 0) {
        return resolve([]);
      }
      var remaining = args.length;
      function res(i, val) {
        try {
          if (val && (typeof val === 'object' || typeof val === 'function')) {
            var then = val.then;
            if (typeof then === 'function') {
              then.call(val, function(val) { res(i, val); }, reject);
              return;
            }
          }
          args[i] = val;
          if (--remaining === 0) {
            resolve(args);
          }
        } catch (ex) {
          reject(ex);
        }
      }
      for (var i = 0; i < args.length; i++) {
        res(i, args[i]);
      }
    });
  };

  PolyfillPromise.resolve = function(value) {
    if (value && typeof value === 'object' && value.constructor === PolyfillPromise) {
      return value;
    }

    return new PolyfillPromise(function(resolve) {
      resolve(value);
    });
  };

  PolyfillPromise.reject = function(value) {
    return new PolyfillPromise(function(resolve, reject) {
      reject(value);
    });
  };

  PolyfillPromise.race = function(values) {
    return new PolyfillPromise(function(resolve, reject) {
      for (var i = 0, len = values.length; i < len; i++) {
        values[i].then(resolve, reject);
      }
    });
  };

  if (!window.Promise) {
    window.Promise = PolyfillPromise;
  }

});
