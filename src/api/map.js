define([
  'weather/api/util/http',
  'weather/api/util/geo',
  'weather/api/polyfill/promise'
], function(http, geo) {

  var Map;

  Map = function(api) {
    this.api = api;
    this.forecastEndpointUrl = this.api.getBaseUrl() + '/maps/forecast.json';
  };

  Map.prototype.layers = function() {
    var self, parameters;

    self = this;
    parameters = forecastParameters({
      timezone: this.api.timezone
    });

    return new Promise(function(resolve, reject) {

      http.jsonp(self.forecastEndpointUrl, parameters).then(function(rawData) {
        var data = reformatData(rawData);
        resolve(data.layers);
      }, reject);

    });
  };

  Map.prototype.locationsFromQuadKey = function(quadKey) {
    var boundingBox,
        parameters;

    boundingBox = geo.quadKeyToBoundingBox(quadKey);
    parameters = forecastParameters({
      top: boundingBox.top,
      left: boundingBox.left,
      bottom: boundingBox.bottom,
      right: boundingBox.right,
      timezone: this.api.timezone
    });

    return promiseWithReformattedLocationsData(this.forecastEndpointUrl, parameters);
  };

  Map.prototype.locationsFromLocationId = function(locationId) {
    var parameters;

    parameters = forecastParameters({
      timezone: this.api.timezone,
      active: locationId,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      suppressActiveLocation: false
    });

    return promiseWithReformattedLocationsData(this.forecastEndpointUrl, parameters);
  };

  function promiseWithReformattedLocationsData(url, parameters) {
    return new Promise(function(resolve, reject) {

      http.jsonp(url, parameters).then(function(rawData) {
        var data;
        data = reformatData(rawData);
        resolve(data.locations);
      }, reject);

    });
  }

  /*
   * Populates forecast parameters with default values, this will be removed
   * once the correct service layer feeds are available.
   */
  function forecastParameters(parameters) {
    var parameter,
        defaults;

    defaults = {
      top: 51.48,
      left: -3.18,
      bottom: 51.48,
      right: -3.18,
      zoom: 9,
      active: 2653822,
      suppressActiveLocation: true
    };

    for (parameter in defaults) {

      if (defaults.hasOwnProperty(parameter)) {
        if (parameters[parameter] === undefined) {
          parameters[parameter] = defaults[parameter];
        }
      }

    }

    return parameters;
  }

  /*
   * Reformats data from the old maps/forecast.json format to the desired
   * format.  This code is intended to be temporary workaround until the new
   * feeds are available.
   */
  function reformatData(data) {
    var locations,
        location,
        locIdMap,
        day,
        timeslot,
        forecast,
        locationIndex,
        hourlyTimeslots,
        dailyTimeslots,
        layers,
        baseMapUrlParts,
        baseLayerVersion;

    locIdMap = {};
    hourlyTimeslots = [];
    dailyTimeslots = [];

    locations = [];
    for (var i = 0; i < data.locations.length; i++) {
      location = data.locations[i];
      locations.push({
        id: location.id,
        name: location.name,
        coordinates: location.coordinates,
        forecasts: {}
      });

      locIdMap[location.locId] = locations.length - 1;
    }

    for (var j = 0; j < data.days.length; j++) {
      day = data.days[j];
      for (var k = 0; k < day.timeslots.length; k++) {
        var newTimeslot;

        timeslot = day.timeslots[k];
        for (var l = 0; l < timeslot.forecasts.length; l++) {
          forecast = timeslot.forecasts[l];
          locationIndex = locIdMap[forecast.locId];
          locations[locationIndex].forecasts[timeslot.utc] = {
            time: forecast.dateTime,
            temperature: {
              c: forecast.temp,
              f: forecast.tempF
            },
            wind: {
              mph: forecast.windSpdMph,
              kph: forecast.windSpdKph,
              direction: forecast.windDir
            }
          };
        }

        newTimeslot = {
          day: day.name,
          dayShort: day.shortName,
          time: timeslot.time,
          duration: Number(timeslot.hours),
          isInitial: timeslot.isInitial,
          utc: timeslot.utc,
          dateTime: timeslot.dateTime
        };

        if (timeslot.hours === '3') {
          hourlyTimeslots.push(newTimeslot);
        }

        if (timeslot.hours === '12') {
          // This line is temporary, to move the timeslots so that there's no gap, as currently
          // the API returns day times as midday, but we show them at the midnight marker
          newTimeslot.dateTime -= 1000 * 60 * 60 * 11;

          dailyTimeslots.push(newTimeslot);
        }

      }

    }

    baseMapUrlParts = data.config.baseMapUrl.split('/');
    baseLayerVersion = baseMapUrlParts[baseMapUrlParts.length - 2];

    layers = {
      base: {
        id: 'base',
        version: baseLayerVersion
      },
      ukCompositeHourly: {
        id: 'uk-composite-hourly',
        version: data.config.version['3'],
        sublayer: 'composite-hourly',
        timeslots: hourlyTimeslots
      },
      ukCompositeDaily: {
        id: 'uk-composite-daily',
        version: data.config.version['12'],
        sublayer: 'composite-daily',
        timeslots: dailyTimeslots
      }
    };

    return {
      layers: layers,
      locations: locations
    };
  }

  return Map;

});
