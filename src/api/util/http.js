define(['weather/api/polyfill/promise'], function() {
  'use strict';

  function encodeParameters(data) {
    var parameters,
        encode,
        key;

    if (typeof data === 'string') {
      return data;
    }

    parameters = [];
    encode = encodeURIComponent;

    for (key in data) {
      if (data.hasOwnProperty(key)) {
        parameters.push(encode(key) + '=' + encode(data[key]));
      }
    }

    return parameters.join('&');

  }

  function jsonp(url, data, options) {

    return new Promise(function(resolve, reject) {
      var script,
          jsonp,
          jsonpCallback;

      options = options || {};

      jsonp = options.jsonp || 'jsoncallback';
      jsonpCallback = options.jsonpCallback || '_weather_jsonp_callback_' + new Date().getTime() + Math.round(100000 * Math.random());

      data[jsonp] = jsonpCallback;

      script = document.createElement('script');
      script.src = url + '?' + encodeParameters(data);

      script.onerror = reject;

      window[jsonpCallback] = function(data) {
        try {
          delete window[jsonpCallback];
        } catch (ex) {
          window[jsonpCallback] = undefined;
        }

        document.body.removeChild(script);
        resolve(data);
      };

      document.body.appendChild(script);

    });

  }

  return {
    jsonp: jsonp
  };

});
