define(function() {

  function clip(value, minValue, maxValue) {
    return Math.min(Math.max(value, minValue), maxValue);
  }

  function mapSizeForZoomLevel(zoomLevel) {
    return 256 << zoomLevel;
  }

  function pixelXYZToLatitudeLongitude(pixelX, pixelY, zoomLevel) {
    var mapSize,
        x,
        y,
        latitude,
        longitude;

    mapSize = mapSizeForZoomLevel(zoomLevel);
    x = (clip(pixelX, 0, mapSize - 1) / mapSize) - 0.5;
    y = 0.5 - (clip(pixelY, 0, mapSize - 1) / mapSize);

    latitude = 90 - 360 * Math.atan(Math.exp(-y * 2 * Math.PI)) / Math.PI;
    longitude = 360 * x;

    return {
      latitude: latitude,
      longitude: longitude
    };
  }

  function tileXYToPixelXY(tileX, tileY) {
    var pixelX,
        pixelY;

    pixelX = tileX * 256;
    pixelY = tileY * 256;

    return {
      x: pixelX,
      y: pixelY
    };
  }

  function quadKeyToTileXYZ(quadKey) {
    var tileX,
        tileY,
        zoomLevel,
        i,
        mask;

    tileX = tileY = 0;
    zoomLevel = quadKey.length;

    for (i = zoomLevel; i > 0; i--) {
      mask = 1 << (i - 1);
      switch (quadKey[zoomLevel - i]) {
        case '0':
          break;

        case '1':
          tileX |= mask;
          break;

        case '2':
          tileY |= mask;
          break;

        case '3':
          tileX |= mask;
          tileY |= mask;
          break;

        default:
          throw new Error('Invalid quad key digit sequence');

      }

    }

    return {
      x: tileX,
      y: tileY,
      z: zoomLevel
    };

  }

  function quadKeyToBoundingBox(quadKey) {
    var tile,
        pixel,
        topLeft,
        bottomRight;

    tile = quadKeyToTileXYZ(quadKey);
    pixel = tileXYToPixelXY(tile.x, tile.y);
    topLeft = pixelXYZToLatitudeLongitude(pixel.x, pixel.y, tile.z);
    bottomRight = pixelXYZToLatitudeLongitude(pixel.x + 256, pixel.y + 256, tile.z);

    return {
      top: topLeft.latitude,
      left: topLeft.longitude,
      bottom: bottomRight.latitude,
      right: bottomRight.longitude
    };

  }

  return {
    quadKeyToBoundingBox: quadKeyToBoundingBox
  };

});
