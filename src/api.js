define(['weather/api/map'], function(Map) {

  var Api;

  var baseUrl = 'http://open.{{environment}}.bbc.co.uk/weather/feeds/{{locale}}';

  Api = function(options) {
    options = options || {};
    this.environment = options.environment || 'live';
    this.locale = options.locale || 'en';
    this.timezone = options.timezone || 'Europe/London';

    this.map = new Map(this);
  };

  Api.prototype.getBaseUrl = function() {
    return baseUrl.replace('{{environment}}', this.environment)
                  .replace('{{locale}}', this.locale);
  };

  return Api;

});
