define(['weather/api/util/geo'], function(geo) {
  'use strict';

  describe('util.geo', function() {

    describe('#quadKeyToBoundingBox', function() {

      it('converts a quad key to a bounding box for quadKey 0', function() {
        expect(geo.quadKeyToBoundingBox('0')).toEqual({
          top: 85.05112877980659,
          left: -180,
          bottom: 0,
          right: 0
        });
      });

      it('converts a quad key to a bounding box for quadKey 1', function() {
        expect(geo.quadKeyToBoundingBox('1')).toEqual({
          top: 85.05112877980659,
          left: 0,
          bottom: 0,
          right: 179.296875
        });
      });

      it('converts a quad key to a bounding box for quadKey 2', function() {
        expect(geo.quadKeyToBoundingBox('2')).toEqual({
          top: 0,
          left: -180,
          bottom: -84.9901001802348,
          right: 0
        });
      });

      it('converts a quad key to a bounding box for quadKey 3', function() {
        expect(geo.quadKeyToBoundingBox('3')).toEqual({
          top: 0,
          left: 0,
          bottom: -84.9901001802348,
          right: 179.296875
        });
      });

    });

  });
});
