define(['weather/api/util/http'], function(http) {

  describe('util.http', function() {

    describe('#jsonp', function() {

      it('returns a promise', function() {
        var promise;
        promise = http.jsonp('/');
        expect(promise instanceof Promise).toBe(true);
      });

    });

  });

});
