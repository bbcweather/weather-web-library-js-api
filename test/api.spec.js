define(['weather/api'], function(Api) {

  describe('api', function() {

    var api;

    describe('constructor', function() {

      it('sets the environment from options', function() {
        api = new Api({ environment: 'test' });

        expect(api.environment).toEqual('test');
      });

      it('sets the locale from options', function() {
        api = new Api({ locale: 'cy' });

        expect(api.locale).toEqual('cy');
      });

      it('sets the timezone from options', function() {
        api = new Api({ timezone: 'America/New_York' });

        expect(api.timezone).toEqual('America/New_York');

      });

    });

  });

});
