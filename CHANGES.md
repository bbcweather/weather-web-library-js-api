Changes
=======

0.0.3 - 2015-05-01
------------------

* Including dayShort and dateTime in timeslots for the drag bar
* Added a temporary fix for the timings of the day/night markers


0.0.2 - 2015-04-13
------------------

* Including day and time strings in timeslots for map locations
* Added method for fetching map location data for a single location


0.0.1 - 2015-03-25
------------------

* Initial release


0.0.0 - 2015-03-05
------------------

* Project creation
